<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>キャリアアンカー診断</title>
</head>
<body>
	<h1>キャリアアンカー診断</h1>

	<c:if test="${not empty errorMessages }">
		<ul>
			<c:forEach items="${errorMessages }" var="message">
				<c:out value="${message}" /><br>
			</c:forEach>
		</ul>
		<c:remove var="errorMessages" scope="session"/>
	</c:if>

	<a href="spreadsheet">前回結果</a>
	<a href="logout">ログアウト</a>

	<p>Q.下記の項目について当てはまると思うものに数値を入力してください。</p>
	<p>まったく違う(1・2・3・4・5)いつもそう思う</p>
	<form action="index.jsp" method="post">
		<table border="1">
			<tr>
				<th>No</th>
				<th>設問</th>
				<!-- <th>分類</th> -->
				<th>回答</th>
			</tr>
			<c:forEach items="${questions}" var="question">
				<tr>
					<td><c:out value="${question.number}" /></td>
					<td><c:out value="${question.text }" /></td>
					<td>
						<input type="radio" name="${question.number }" value="1">1
						<input type="radio" name="${question.number }" value="2">2
						<input type="radio" name="${question.number }" value="3">3
						<input type="radio" name="${question.number }" value="4">4
						<input type="radio" name="${question.number }" value="5">5
						<input type="radio" name="${question.number }" value="6">6
					</td>
				</tr>
			</c:forEach>
		</table>
		<input type="submit" value="登録">
	</form>
</body>
</html>