package beans;

import java.util.Date;

public class User {
	private int id;
	private String loginId;
	private String name;
	private String password;
	private Date createdData;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Date getCreatedData() {
		return createdData;
	}
	public void setCreatedData(Date createdData) {
		this.createdData = createdData;
	}
}
