package beans;

public class Spreadsheet {
	private int id;
	private String userId;
	private int q1;
	private int q2;
	private int q3;
	private int q4;
	private int q5;
	private int q6;
	private int q7;
	private int q8;
	private int q9;
	private int q10;
	private int q11;
	private int q12;
	private int q13;
	private int q14;
	private int q15;
	private int q16;
	private int q17;
	private int q18;
	private int q19;
	private int q20;
	private int q21;
	private int q22;
	private int q23;
	private int q24;
	private int q25;
	private int q26;
	private int q27;
	private int q28;
	private int q29;
	private int q30;
	private int q31;
	private int q32;
	private int q33;
	private int q34;
	private int q35;
	private int q36;
	private int q37;
	private int q38;
	private int q39;
	private int q40;
	private int sum1;
	private int sum2;
	private int sum3;
	private int sum4;
	private int sum5;
	private int sum6;
	private int sum7;
	private int sum8;
	private int tf;
	private int gm;
	private int au;
	private int se;
	private int ec;
	private int sv;
	private int ch;
	private int ls;



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public int getQ1() {
		return q1;
	}

	public void setQ1(int q1) {
		this.q1 = q1;
	}

	public int getQ2() {
		return q2;
	}

	public void setQ2(int q2) {
		this.q2 = q2;
	}

	public int getQ3() {
		return q3;
	}

	public void setQ3(int q3) {
		this.q3 = q3;
	}

	public int getQ4() {
		return q4;
	}

	public void setQ4(int q4) {
		this.q4 = q4;
	}

	public int getQ5() {
		return q5;
	}

	public void setQ5(int q5) {
		this.q5 = q5;
	}

	public int getQ6() {
		return q6;
	}

	public void setQ6(int q6) {
		this.q6 = q6;
	}

	public int getQ7() {
		return q7;
	}

	public void setQ7(int q7) {
		this.q7 = q7;
	}

	public int getQ8() {
		return q8;
	}

	public void setQ8(int q8) {
		this.q8 = q8;
	}

	public int getQ9() {
		return q9;
	}

	public void setQ9(int q9) {
		this.q9 = q9;
	}

	public int getQ10() {
		return q10;
	}

	public void setQ10(int q10) {
		this.q10 = q10;
	}

	public int getQ11() {
		return q11;
	}

	public void setQ11(int q11) {
		this.q11 = q11;
	}

	public int getQ12() {
		return q12;
	}

	public void setQ12(int q12) {
		this.q12 = q12;
	}

	public int getQ13() {
		return q13;
	}

	public void setQ13(int q13) {
		this.q13 = q13;
	}

	public int getQ14() {
		return q14;
	}

	public void setQ14(int q14) {
		this.q14 = q14;
	}

	public int getQ15() {
		return q15;
	}

	public void setQ15(int q15) {
		this.q15 = q15;
	}

	public int getQ16() {
		return q16;
	}

	public void setQ16(int q16) {
		this.q16 = q16;
	}

	public int getQ17() {
		return q17;
	}

	public void setQ17(int q17) {
		this.q17 = q17;
	}

	public int getQ18() {
		return q18;
	}

	public void setQ18(int q18) {
		this.q18 = q18;
	}

	public int getQ19() {
		return q19;
	}

	public void setQ19(int q19) {
		this.q19 = q19;
	}

	public int getQ20() {
		return q20;
	}

	public void setQ20(int q20) {
		this.q20 = q20;
	}

	public int getQ21() {
		return q21;
	}

	public void setQ21(int q21) {
		this.q21 = q21;
	}

	public int getQ22() {
		return q22;
	}

	public void setQ22(int q22) {
		this.q22 = q22;
	}

	public int getQ23() {
		return q23;
	}

	public void setQ23(int q23) {
		this.q23 = q23;
	}

	public int getQ24() {
		return q24;
	}

	public void setQ24(int q24) {
		this.q24 = q24;
	}

	public int getQ25() {
		return q25;
	}

	public void setQ25(int q25) {
		this.q25 = q25;
	}

	public int getQ26() {
		return q26;
	}

	public void setQ26(int q26) {
		this.q26 = q26;
	}

	public int getQ27() {
		return q27;
	}

	public void setQ27(int q27) {
		this.q27 = q27;
	}

	public int getQ28() {
		return q28;
	}

	public void setQ28(int q28) {
		this.q28 = q28;
	}

	public int getQ29() {
		return q29;
	}

	public void setQ29(int q29) {
		this.q29 = q29;
	}

	public int getQ30() {
		return q30;
	}

	public void setQ30(int q30) {
		this.q30 = q30;
	}

	public int getQ31() {
		return q31;
	}

	public void setQ31(int q31) {
		this.q31 = q31;
	}

	public int getQ32() {
		return q32;
	}

	public void setQ32(int q32) {
		this.q32 = q32;
	}

	public int getQ33() {
		return q33;
	}

	public void setQ33(int q33) {
		this.q33 = q33;
	}

	public int getQ34() {
		return q34;
	}

	public void setQ34(int q34) {
		this.q34 = q34;
	}

	public int getQ35() {
		return q35;
	}

	public void setQ35(int q35) {
		this.q35 = q35;
	}

	public int getQ36() {
		return q36;
	}

	public void setQ36(int q36) {
		this.q36 = q36;
	}

	public int getQ37() {
		return q37;
	}

	public void setQ37(int q37) {
		this.q37 = q37;
	}

	public int getQ38() {
		return q38;
	}

	public void setQ38(int q38) {
		this.q38 = q38;
	}

	public int getQ39() {
		return q39;
	}

	public void setQ39(int q39) {
		this.q39 = q39;
	}

	public int getQ40() {
		return q40;
	}

	public void setQ40(int q40) {
		this.q40 = q40;
	}

	public int getSum1() {
		return sum1;
	}

	public void setSum1(int sum1) {
		this.sum1 = sum1;
	}

	public int getSum2() {
		return sum2;
	}

	public void setSum2(int sum2) {
		this.sum2 = sum2;
	}

	public int getSum3() {
		return sum3;
	}

	public void setSum3(int sum3) {
		this.sum3 = sum3;
	}

	public int getSum4() {
		return sum4;
	}

	public void setSum4(int sum4) {
		this.sum4 = sum4;
	}

	public int getSum5() {
		return sum5;
	}

	public void setSum5(int sum5) {
		this.sum5 = sum5;
	}

	public int getSum6() {
		return sum6;
	}

	public void setSum6(int sum6) {
		this.sum6 = sum6;
	}

	public int getSum7() {
		return sum7;
	}

	public void setSum7(int sum7) {
		this.sum7 = sum7;
	}

	public int getSum8() {
		return sum8;
	}

	public void setSum8(int sum8) {
		this.sum8 = sum8;
	}
	public int getTf() {
		return tf;
	}

	public void setTf(int tf) {
		this.tf = tf;
	}

	public int getGm() {
		return gm;
	}

	public void setGm(int gm) {
		this.gm = gm;
	}

	public int getAu() {
		return au;
	}

	public void setAu(int au) {
		this.au = au;
	}

	public int getSe() {
		return se;
	}

	public void setSe(int se) {
		this.se = se;
	}

	public int getEc() {
		return ec;
	}

	public void setEc(int ec) {
		this.ec = ec;
	}

	public int getSv() {
		return sv;
	}

	public void setSv(int sv) {
		this.sv = sv;
	}

	public int getCh() {
		return ch;
	}

	public void setCh(int ch) {
		this.ch = ch;
	}

	public int getLs() {
		return ls;
	}

	public void setLs(int ls) {
		this.ls = ls;
	}

}
