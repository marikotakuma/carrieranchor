package service;


import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Question;
import dao.QuestionDao;

public class QuestionService {
	public List<Question> getQuestion() {

		Connection connection = null;
		try {
			connection = getConnection();

			QuestionDao questionDao = new QuestionDao();
			List<Question> ret = questionDao.getQuestion(connection);

			commit(connection);

			return ret;
		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}catch(Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}

	public void register(String userId, List<String> answerId) {

		Connection connection = null;
		try {
			connection = getConnection();

			QuestionDao questionDao = new QuestionDao();
			questionDao.inset(connection, userId, answerId);



			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
