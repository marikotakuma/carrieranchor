package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import beans.Spreadsheet;
import dao.SpreadsheetDao;

public class SpreadsheetService {
	public List<Spreadsheet> getSpreadsheet(String loginId) {

		Connection connection = null;
		try {
			connection = getConnection();

			SpreadsheetDao spreadsheetDao = new SpreadsheetDao();
			List<Spreadsheet> ret = spreadsheetDao.getSpreadsheet(connection, loginId);

			//合計値の計算
			ret.get(0).setSum1(ret.get(0).getQ1() + ret.get(0).getQ9() + ret.get(0).getQ17() + ret.get(0).getQ25()
					+ ret.get(0).getQ33());

			ret.get(0).setSum2(ret.get(0).getQ2() + ret.get(0).getQ10() + ret.get(0).getQ18() + ret.get(0).getQ26()
					+ ret.get(0).getQ34());

			ret.get(0).setSum3(ret.get(0).getQ3() + ret.get(0).getQ11() + ret.get(0).getQ19() + ret.get(0).getQ27()
					+ ret.get(0).getQ35());

			ret.get(0).setSum4(ret.get(0).getQ4() + ret.get(0).getQ12() + ret.get(0).getQ20() + ret.get(0).getQ28()
					+ ret.get(0).getQ36());

			ret.get(0).setSum5(ret.get(0).getQ5() + ret.get(0).getQ13() + ret.get(0).getQ21() + ret.get(0).getQ29()
					+ ret.get(0).getQ37());

			ret.get(0).setSum6(ret.get(0).getQ6() + ret.get(0).getQ14() + ret.get(0).getQ22() + ret.get(0).getQ30()
					+ ret.get(0).getQ38());

			ret.get(0).setSum7(ret.get(0).getQ7() + ret.get(0).getQ15() + ret.get(0).getQ23() + ret.get(0).getQ31()
					+ ret.get(0).getQ39());

			ret.get(0).setSum8(ret.get(0).getQ8() + ret.get(0).getQ16() + ret.get(0).getQ24() + ret.get(0).getQ32()
					+ ret.get(0).getQ40());


			ret.add(ret.get(0));

			Map<Integer, Integer> sorts = new HashMap<Integer, Integer>();

			sorts.put(1, ret.get(1).getSum1());
			sorts.put(2, ret.get(1).getSum2());
			sorts.put(3, ret.get(1).getSum3());
			sorts.put(4, ret.get(1).getSum4());
			sorts.put(5, ret.get(1).getSum5());
			sorts.put(6, ret.get(1).getSum6());
			sorts.put(7, ret.get(1).getSum7());
			sorts.put(8, ret.get(1).getSum8());



			List<Entry<Integer, Integer>> sort = new ArrayList<Entry<Integer, Integer>>(sorts.entrySet());

			Collections.sort(sort, new Comparator<Entry<Integer, Integer>>() {
	            //compareを使用して値を比較する
	            public int compare(Entry<Integer, Integer> obj1, Entry<Integer, Integer> obj2)
	            {
	                //降順
	                return obj2.getValue().compareTo(obj1.getValue());
	            }
	        });

			for(int i = 0; i < 2; i++) {
				switch (sort.get(i).getKey()) {
				case 1:
					ret.get(0).setTf(1);
					break;
				case 2:
					ret.get(0).setGm(1);
					break;
				case 3:
					ret.get(0).setAu(1);
					break;
				case 4:
					ret.get(0).setSe(1);
					break;
				case 5:
					ret.get(0).setEc(1);
					break;
				case 6:
					ret.get(0).setSv(1);
					break;
				case 7:
					ret.get(0).setCh(1);
					break;
				case 8:
					ret.get(0).setLs(1);
					break;
				}
			}

			for(int i = 2; i < 6; i++) {
				switch (sort.get(i).getKey()) {
				case 1:
					ret.get(0).setTf(0);
					break;
				case 2:
					ret.get(0).setGm(0);
					break;
				case 3:
					ret.get(0).setAu(0);
					break;
				case 4:
					ret.get(0).setSe(0);
					break;
				case 5:
					ret.get(0).setEc(0);
					break;
				case 6:
					ret.get(0).setSv(0);
					break;
				case 7:
					ret.get(0).setCh(0);
					break;
				case 8:
					ret.get(0).setLs(0);
					break;
				}
			}
				for(int i = 6; i < 8; i++) {
					switch (sort.get(i).getKey()) {
					case 1:
						ret.get(0).setTf(2);
						break;
					case 2:
						ret.get(0).setGm(2);
						break;
					case 3:
						ret.get(0).setAu(2);
						break;
					case 4:
						ret.get(0).setSe(2);
						break;
					case 5:
						ret.get(0).setEc(2);
						break;
					case 6:
						ret.get(0).setSv(2);
						break;
					case 7:
						ret.get(0).setCh(2);
						break;
					case 8:
						ret.get(0).setLs(2);
						break;
					}
				}
			ret.add(ret.get(0));

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
