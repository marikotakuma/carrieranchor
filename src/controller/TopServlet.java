package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Question;
import beans.User;
import service.QuestionService;

/**
 * Servlet implementation class TopServlet
 */
@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<Question> questions = new QuestionService().getQuestion();
		request.setAttribute("questions", questions);

		request.getRequestDispatcher("top.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();

		QuestionService question = new QuestionService();

		User user = (User) session.getAttribute("loginUser");

		List<String> answer = new ArrayList<String>();
		List<String> message = new ArrayList<String>();

		String userId = user.getLoginId();

		if(isValid(request, message) == true) {
			for (int i = 0; i < 40; i++) {

				String number = request.getParameter(Integer.toString(i + 1));

				answer.add(number);

			}

		question.register(userId, answer);

		response.sendRedirect("spreadsheet");
		}else{
			session.setAttribute("errorMessages", message);
			response.sendRedirect("index.jsp");
		}
	}

	public boolean isValid(HttpServletRequest request, List<String> message) {

		List<String> q = new ArrayList<String>();

		for(int i = 0; i < 40; i++) {

			String number = request.getParameter(Integer.toString(i + 1));
			q.add(number);
		}

		for(int i = 0; i < 40; i++) {
			if(q.get(i) == null){
				message.add("No" + (i+1) + "が未入力です");
			}else if(q.get(i) != null){
				continue;
			}
		}

		if(message.size() == 0) {
			return true;
		}else{
			return false;
		}
	}
}
