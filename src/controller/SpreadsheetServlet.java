package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Spreadsheet;
import beans.User;
import service.SpreadsheetService;

@WebServlet(urlPatterns = {"/spreadsheet"})
public class SpreadsheetServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();

		User user  = (User) session.getAttribute("loginUser");

		String loginId = user.getLoginId();

		List<Spreadsheet> spreadsheet =  new SpreadsheetService().getSpreadsheet(loginId);

		request.setAttribute("spreadsheet", spreadsheet);

		request.getRequestDispatcher("spreadsheet.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}
}
