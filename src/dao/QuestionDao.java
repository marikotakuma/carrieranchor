package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Question;
import exception.SQLRuntimeException;

public class QuestionDao {

	public List<Question> getQuestion(Connection connection) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM questions";

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<Question> ret = toQuestionList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Question> toQuestionList(ResultSet rs) throws SQLException {

		List<Question> ret = new ArrayList<Question>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String number = rs.getString("question_number");
				String text = rs.getString("text");
				String type = rs.getString("type");

				Question question = new Question();
				question.setId(id);
				question.setNumber(number);
				question.setText(text);
				question.setType(type);

				ret.add(question);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public void inset(Connection connection, String userId, List<String> answerId) {


		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO results ( ");
			sql.append("user_id");

			for(int i = 0; i < answerId.size(); i++) {

				String j = ", q" + (i + 1);

				sql.append(j);
			}
			sql.append(") VALUES (");
			sql.append(" ?"); // user_id

			for(String answer: answerId) {

				String s = "," + answer;

				sql.append(s); // answerId
			}
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, userId);


			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}
