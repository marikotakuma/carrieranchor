package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Spreadsheet;
import exception.SQLRuntimeException;

public class SpreadsheetDao {
	public List<Spreadsheet> getSpreadsheet(Connection connection, String loginId) {

		PreparedStatement ps = null;
//		PreparedStatement ps2 = null;
		try {
			String sql = "SELECT * FROM results WHERE id = ( SELECT MAX(id) FROM results) AND user_id = ?";

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, loginId);

			ResultSet rs = ps.executeQuery();

/*			StringBuilder sql2 = new StringBuilder();

			sql2.append("select typ, score from ( ");
			sql2.append("select \"TF\" as typ, sum(q1+q9+q17+q25+q33) as score ");
			sql2.append("from results where id = (SELECT MAX(id) FROM results) AND user_ id = ? ");
			sql2.append("union all ");
			sql2.append("select \"GM\" as typ, sum(q2+q10+q18+q26+q34) as score ");
			sql2.append("from results where id = (SELECT MAX(id) FROM results) AND user_ id = ? ");
			sql2.append("union all ");
			sql2.append("select \"AU\" as typ, sum(q3+q11+q19+q27+q35) as score ");
			sql2.append("from results where id = (SELECT MAX(id) FROM results) AND user_ id = ? ");
			sql2.append("union all ");
			sql2.append("select \"SE\" as typ, sum(q4+q12+q20+q28+q36) as score ");
			sql2.append("from results where id = (SELECT MAX(id) FROM results) AND user_ id = ? ");
			sql2.append("union all ");
			sql2.append("select \"EC\" as typ, sum(q5+q13+q21+q29+q37) as score ");
			sql2.append("from results where id = (SELECT MAX(id) FROM results) AND user_ id = ? ");
			sql2.append("union all ");
			sql2.append("select \"SV\" as typ, sum(q6+q14+q22+q30+q38) as score ");
			sql2.append("from results where id = (SELECT MAX(id) FROM results) AND user_ id = ? ");
			sql2.append("union all ");
			sql2.append("select \"CH\" as typ, sum(q7+q15+q23+q31+q39) as score ");
			sql2.append("from results where id = (SELECT MAX(id) FROM results) AND user_ id = ? ");
			sql2.append("union all ");
			sql2.append("select \"LS\" as typ, sum(q8+q16+q24+q32+q40) as score ");
			sql2.append("from results where id = (SELECT MAX(id) FROM results) AND user_ id = ? ");
			sql2.append(") A ");
			sql2.append("ORDER BY a.score DESC;");


			ps2 = connection.prepareStatement(sql2.toString());
			ps2.setString(1, loginId);
			ps2.setString(2, loginId);
			ps2.setString(3, loginId);
			ps2.setString(4, loginId);
			ps2.setString(5, loginId);
			ps2.setString(6, loginId);
			ps2.setString(7, loginId);
			ps2.setString(8, loginId);


			ResultSet rs2 = ps2.executeQuery();
*/

			List<Spreadsheet> ret = toSpreadsheet(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	private List<Spreadsheet> toSpreadsheet(ResultSet rs) throws SQLException {

		List<Spreadsheet> ret = new ArrayList<Spreadsheet>();

		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String userId = rs.getString("user_id");
				int q1 = rs.getInt("q1");
				int q2 = rs.getInt("q2");
				int q3 = rs.getInt("q3");
				int q4 = rs.getInt("q4");
				int q5 = rs.getInt("q5");
				int q6 = rs.getInt("q6");
				int q7 = rs.getInt("q7");
				int q8 = rs.getInt("q8");
				int q9 = rs.getInt("q9");
				int q10 = rs.getInt("q10");
				int q11 = rs.getInt("q11");
				int q12 = rs.getInt("q12");
				int q13 = rs.getInt("q13");
				int q14 = rs.getInt("q14");
				int q15 = rs.getInt("q15");
				int q16 = rs.getInt("q16");
				int q17 = rs.getInt("q17");
				int q18 = rs.getInt("q18");
				int q19 = rs.getInt("q19");
				int q20 = rs.getInt("q20");
				int q21 = rs.getInt("q21");
				int q22 = rs.getInt("q22");
				int q23 = rs.getInt("q23");
				int q24 = rs.getInt("q24");
				int q25 = rs.getInt("q25");
				int q26 = rs.getInt("q26");
				int q27 = rs.getInt("q27");
				int q28 = rs.getInt("q28");
				int q29 = rs.getInt("q29");
				int q30 = rs.getInt("q30");
				int q31 = rs.getInt("q31");
				int q32 = rs.getInt("q32");
				int q33 = rs.getInt("q33");
				int q34 = rs.getInt("q34");
				int q35 = rs.getInt("q35");
				int q36 = rs.getInt("q36");
				int q37 = rs.getInt("q37");
				int q38 = rs.getInt("q38");
				int q39 = rs.getInt("q39");
				int q40 = rs.getInt("q40");



				Spreadsheet spreadsheet = new Spreadsheet();
				spreadsheet.setId(id);
				spreadsheet.setUserId(userId);
				spreadsheet.setQ1(q1);
				spreadsheet.setQ2(q2);
				spreadsheet.setQ3(q3);
				spreadsheet.setQ4(q4);
				spreadsheet.setQ5(q5);
				spreadsheet.setQ6(q6);
				spreadsheet.setQ7(q7);
				spreadsheet.setQ8(q8);
				spreadsheet.setQ9(q9);
				spreadsheet.setQ10(q10);
				spreadsheet.setQ11(q11);
				spreadsheet.setQ12(q12);
				spreadsheet.setQ13(q13);
				spreadsheet.setQ14(q14);
				spreadsheet.setQ15(q15);
				spreadsheet.setQ16(q16);
				spreadsheet.setQ17(q17);
				spreadsheet.setQ18(q18);
				spreadsheet.setQ19(q19);
				spreadsheet.setQ20(q20);
				spreadsheet.setQ21(q21);
				spreadsheet.setQ22(q22);
				spreadsheet.setQ23(q23);
				spreadsheet.setQ24(q24);
				spreadsheet.setQ25(q25);
				spreadsheet.setQ26(q26);
				spreadsheet.setQ27(q27);
				spreadsheet.setQ28(q28);
				spreadsheet.setQ29(q29);
				spreadsheet.setQ30(q30);
				spreadsheet.setQ31(q31);
				spreadsheet.setQ32(q32);
				spreadsheet.setQ33(q33);
				spreadsheet.setQ34(q34);
				spreadsheet.setQ35(q35);
				spreadsheet.setQ36(q36);
				spreadsheet.setQ37(q37);
				spreadsheet.setQ38(q38);
				spreadsheet.setQ39(q39);
				spreadsheet.setQ40(q40);

				ret.add(spreadsheet);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}
